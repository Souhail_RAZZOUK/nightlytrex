/// <reference path="../../../bin/assets/components/backand-vanilla-sdk/dist/backand.d.ts" />

(function () {

  const init = () => backand.init({
    appName: "NightlyTrex",
    signUpToken: "ac4b1683-b2eb-4af5-9c60-9063cb9056a1",
    anonymousToken: "1d942082-3bf6-497c-be57-41d01b3036d4",
    runSocket: true
  });

  const signupUser = (user: any) => backand.signup(user.firstName, user.lastName, user.email, user.password, user.confirmPassword, {
    login: user.login,
    avatar: user.avatar,
    date_joined: user.date_joined,
    country: user.country,
    birthday: user.birthday
  });

  const loginUser = (userName: string, password: string) => backand.signin(userName, password).then(() => {
    return NightlyTrex.DataServices.getCurrentUser()
      .then((userDetails: any) => {
        NightlyTrex.user.id = userDetails.data.id;
        NightlyTrex.user.avatar = userDetails.data.avatar;
        NightlyTrex.user.name = userDetails.data.login;
        NightlyTrex.user.email = userDetails.data.email;
        NightlyTrex.user.country = userDetails.data.country;
        NightlyTrex.user.birthday = userDetails.data.birthday;
      });
  });

  const logoutUser = () => backand.signout();

  const getCurrentUser = () => backand.user.getUserDetails()
    .then((userDetails: any) => {
      return (userDetails.data) ? NightlyTrex.DataServices.getUser(userDetails.data.userId) : { data: false };
    });

  const getUser = (id: string) => backand.object.getOne("users", id, {});

  const updateUser = (id: string, newUser: any) => backand.object.update("users", id, newUser, { returnData: true });

  const changeUserPassword = (oldPassword: string, newPassword: string): any => backand.changePassword(oldPassword, newPassword);

  const requestResetPassword = (email: string): any => backand.requestResetPassword(email);

  const resetPassword = (newPassword: string, resetToken: string) => backand.resetPassword(newPassword, resetToken);

  const checkWinner = () => {
    let params = {
      deep: true,
      filter: [backand.helpers.filter.create("redumped", backand.helpers.filter.operators.boolean.equals, "true")],
    };

    return backand.object.getList("winners", params);
  };

  const getUserScores = (id: string) => backand.object.getList("hi_scores", { filter: [backand.helpers.filter.create("user", backand.helpers.filter.operators.relation.in, id)] });

  const getCurrentUserScores = () => NightlyTrex.DataServices.getUserScores(NightlyTrex.user.id);

  const getScoresChunk = (page: number) => {

    let params = {
      deep: true,
      sort: backand.helpers.sort.create("score", backand.helpers.sort.orders.desc),
      filter: backand.helpers.filter.create("date_recorded", backand.helpers.filter.operators.date.greaterThanOrEqualsTo, moment(moment().utc().format("w"), "w").utc().format("YYYY-MM-DD")),
      pageSize: 20,
      pageNumber: page
    };

    return backand.object.getList("hi_scores", params);

  };

  const getScores = (): any => {
    let params = {
      deep: true,
      sort: backand.helpers.sort.create("score", backand.helpers.sort.orders.desc),
      filter: backand.helpers.filter.create("date_recorded", backand.helpers.filter.operators.date.greaterThanOrEqualsTo, moment(moment().utc().format("w"), "w").utc().format("YYYY-MM-DD")),
    };

    return backand.object.getList("hi_scores", params);
  };

  const saveScore = (hiScore: number): any => {
    return NightlyTrex.DataServices.getCurrentUser()
      .then((userDetails: any) => {
        return backand.object.create("hi_scores", { score: hiScore, date_recorded: new Date(), user: userDetails.data }, {});
      });
  };

  const getCountries = (): WinJS.Promise<any> => {
    return WinJS.xhr({
      type: "GET",
      url: "https://restcountries-v1.p.mashape.com/all",
      responseType: "json",
      headers: {
        "X-Mashape-Key": "mXjVY4iVtAmshv8ZPx4DW5Iy1tkep1GNIlXjsnBEFQs8NW8AJG",
        "Accept": "application/json"
      }
    });
  };

  const getProducts = (): WinJS.Promise<any> => {
    return WinJS.xhr({
      type: "GET",
      url: "/assets/json/products.json",
      responseType: "json",
      headers: {
        "Accept": "application/json"
      }
    });
  };

  const getFrontProducts = (): WinJS.Promise<any> => {
    return WinJS.xhr({
      type: "GET",
      url: "/assets/json/front-products.json",
      responseType: "json",
      headers: {
        "Accept": "application/json"
      }
    });
  };

  WinJS.Namespace.define("NightlyTrex", {
    DataServices: {
      init: init,
      signupUser: signupUser,
      loginUser: loginUser,
      logoutUser: logoutUser,
      getCurrentUser: getCurrentUser,
      getUser: getUser,
      updateUser: updateUser,
      changeUserPassword: changeUserPassword,
      requestResetPassword: requestResetPassword,
      resetPassword: resetPassword,
      checkWinner: checkWinner,
      getScores: getScores,
      getScoresChunk: getScoresChunk,
      getUserScores: getUserScores,
      getCurrentUserScores: getCurrentUserScores,
      saveScore: saveScore,
      getCountries: getCountries,
      getProducts: getProducts,
      getFrontProducts: getFrontProducts
    }
  });

})();