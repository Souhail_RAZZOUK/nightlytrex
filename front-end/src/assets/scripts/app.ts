(function () {

  let scenarios = [
    { url: "/views/splash.html", title: "Nightly T-REX | Loading ..." },
    { url: "/views/account/create.html", title: "Nightly T-REX | Create your Account" },
    { url: "/views/account/connect.html", title: "Nightly T-REX | Login to your Account" },
    { url: "/views/account/forgot.html", title: "Nightly T-REX | Recover your password" },
    { url: "/views/game.html", title: "Nightly T-REX | Home", state: { secured: true } },
    { url: "/views/scores.html", title: "Nightly T-REX | Scores Board" },
    { url: "/views/about.html", title: "Nightly T-REX | About Nightly T-REX" },
    { url: "/views/article.html", title: "Nightly T-REX | Privacy Policy", state: { article: "privacypolicy", secured: false } },
    { url: "/views/article.html", title: "Nightly T-REX | Termes Of Services", state: { article: "termesofservices", secured: false } },
    { url: "/views/account/profile.html", title: "Nightly T-REX | My Profile", state: { secured: true } },
    { url: "/views/account/profile.edit.html", title: "Nightly T-REX | My Profile", state: { secured: true } },
    { url: "/views/account/profile.change-password.html", title: "Nightly T-REX | Change account password", state: { secured: true } },
    { url: "/views/shop/products.html", title: "Nightly T-REX | Shop" }
  ];


  let user = WinJS.Binding.as({
    id: 0,
    name: "UserLogin",
    avatar: "/assets/images/user.png",
    scores: new WinJS.Binding.List(),
    email: "",
    country: "",
    birthday: new Date(),
    hiScore: 0
  });

  let frontProductsData = [
    {
      "type": "Mug",
      "picture": "/assets/images/products/front/mug-1.png"
    },
    {
      "type": "T-shirt",
      "picture": "/assets/images/products/front/t-shirt-3.png"
    },
    {
      "type": "Phone Case",
      "picture": "/assets/images/products/front/phone-case-2.png"
    }
  ];

  let scores = WinJS.Binding.as({
    data: new WinJS.Binding.List()
  });

  function activated(eventObject: any) {
    if (eventObject.detail.kind === "Windows.Launch") {
      eventObject.setPromise(WinJS.UI.processAll().then(function () {
        let scenario = scenarios[0];
        return MajesticWaffle.Navigation.navigate(scenario);
      }));
    }
  }

  const renderPage = (hostElement: HTMLElement, url: string, state?: any): WinJS.IPromise<any> => {
    hostElement.winControl && hostElement.winControl.unload && hostElement.winControl.unload();
    WinJS.Utilities.empty(hostElement);
    WinJS.Utilities.query(".win-clickeater").forEach((elem) => { elem.parentElement.removeChild(elem); });
    return WinJS.UI.Pages.render(url, hostElement, state)
      .then(function () {
        WinJS.UI.Animation.enterPage(hostElement);
        WinJS.Application.sessionState.lastUrl = url;
      });
  };

  const randomDialogChoice = (max: number): number => {
    let choice = parseInt((Math.random() * 10).toString());
    return choice < max ? choice : randomDialogChoice(max);
  };

  MajesticWaffle.UI.controlsPath = "/assets/components/majestic-waffle";

  NightlyTrex.DataServices.init();

  WinJS.Application.addEventListener("activated", activated, false);

  WinJS.Navigation.addEventListener("navigated", function (eventObject: any) {
    let url = eventObject.detail.location;
    let state = eventObject.detail.state;
    let host = document.getElementById("app");
    let dialogs = WinJS.Utilities.query("[data-win-control='WinJS.UI.ContentDialog']");

    for (let dialog of dialogs) {
      dialog.winControl.hide();
    }

    if (state && state.secured && !!state.secured) {
      return NightlyTrex.DataServices.getCurrentUser()
        .then((userDetails: any) => {
          eventObject.detail.setPromise(renderPage(host, url, state));
        })
        .catch((error: any) => {
          console.log(error);
          new MajesticWaffle.UI.Alert(null, { text: "You need to be logged in order to perform this action, please use your credentials to log in or create a new account", type: "danger", duration: 5000 });
          MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[0]);
        });
    }
    eventObject.detail.setPromise(renderPage(host, url, state));
  });

  WinJS.Namespace.define("NightlyTrex", {
    runner: new Object(),
    user: user,
    noRewardsDialog: null,
    likePageDialog: null,
    goShopDialog: null,
    gameOverDialogs: [],
    randomDialogChoice: randomDialogChoice,
    scenarios: scenarios,
    countries: [],
    scores: scores,
    Shop: {
      products: new WinJS.Binding.List(),
      frontProducts: new WinJS.Binding.List(frontProductsData)
    }
  });

  validate.extend(validate.validators.datetime, {
    parse: function (value: any, options: any): any {
      return +moment.utc(value);
    },
    format: function (value: any, options: any): any {
      let format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
      return moment.utc(value).format(format);
    }
  });


})();

WinJS.Application.start();

let kiipInstance = new Kiip("9110ac06617f56f85a3621eae8e1ef6f", (unit: any) => {
  if (unit) {
    // User earned a reward, show it
    unit.show();
  } else {
    // User did not earn a reward... do something else
    NightlyTrex.noRewardsDialog.winControl.show();
    console.log(unit, "No rewards are available");
  }
});

//  tslint ignore:start
function addLoadEvent(func: any): void {
  let oldonload: Function = window.onload;
  if (typeof window.onload !== "function") {
    window.onload = func;
  } else {
    window.onload = function () {
      if (oldonload) {
        oldonload();
      }
      func();
    };
  }
}
// call plugin function after DOM ready
addLoadEvent(function () {
  outdatedBrowser({
    bgColor: "#f25648",
    color: "#ffffff",
    lowerThan: "IE10",
    languagePath: "/assets/components/outdated-browser/outdatedbrowser/lang/en.html"
  });
});
//  tslint ignore:end