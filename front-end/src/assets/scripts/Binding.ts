(function () {

  const displayDatesBind = (source: any, sourceProperty: any, destination: any, destinationProperty: any) => destination[destinationProperty[0]] = moment(source[sourceProperty[0]]).utc().format("DD/MM/YYYY");

  WinJS.Namespace.define("NightlyTrex.Binding", {
    displayDatesBind: displayDatesBind
  });

  WinJS.Utilities.markSupportedForProcessing(NightlyTrex.Binding.displayDatesBind);

})();