let article = WinJS.UI.Pages.define("/views/article.html", {
  ready: function (elem: HTMLElement, options?: any): void {
    let contentEl = document.getElementById("content");

    const extractArticleTitleFromURL = (url: string): string => {
      let variables = url.slice(url.indexOf("?") + 1);
      let articleName = variables.slice(variables.indexOf("=") + 1);
      return articleName;
    };

    let articleName = WinJS.Navigation.state.article;

    WinJS.xhr({
      type: "GET",
      url: `/assets/md/${articleName}.md`,
      responseType: "text",
    }).then((res) => {
      contentEl.winControl.setContent(res.response);
    });

  }
});