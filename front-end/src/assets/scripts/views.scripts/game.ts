(function () {

  let game = WinJS.UI.Pages.define("/views/game.html", {
    ready: function (elem: HTMLElement, options?: any): void {

      let self = this;
      let appBar = document.getElementById("game-appbar").winControl;
      let saveScoreDialog = document.getElementById("save-score-dialog").winControl;
      let aboutDialog = document.getElementById("about-dialog").winControl;
      let userMenu = document.getElementById("user-menu").winControl;
      let userCreditsTemplate = document.getElementById("user-credits-template").winControl;
      let hiScoreTemplate = document.getElementById("hi-score-template").winControl;
      let userMenuAnch = document.getElementById("user-credits-menu");
      let hiScoreContainer = document.getElementById("hi-score");
      let commands = {
        playing: [
          appBar.getCommandById("cmdPause"),
          appBar.getCommandById("cmdRestart")
        ],
        paused: [
          appBar.getCommandById("cmdPlay"),
          appBar.getCommandById("cmdRestart")
        ],
        gameOver: [
          appBar.getCommandById("cmdSaveScore"),
          appBar.getCommandById("cmdRestart")
        ]
      };

      let tips = {
        start: `Start your game by taping/clicking the T-Rex, or use the up arrow <span class="symbol">${WinJS.UI.AppBarIcon.upload}</span>, more commands are down`,
        playing: `Avoid obstacles, Cactus and flying Pterodactyl by [Jump: Tap/Click the T-Rex or up arrow <span class="symbol">${WinJS.UI.AppBarIcon.upload}</span>; Duck: down arrow <span class="symbol">${WinJS.UI.AppBarIcon.download}</span>]`,
        pause: `The game is paused, resume by using the play button <span class="symbol">${WinJS.UI.AppBarIcon.play}</span>`,
        restart: `Restart...`,
        gameOver: `The game is over, restart by using the restart button <span class="symbol">${WinJS.UI.AppBarIcon.refresh}</span>, you can save your score by using the save button <span class="symbol">${WinJS.UI.AppBarIcon.save}</span>`,
      };

      let tipsElement = WinJS.Utilities.query("#tips p")[0];

      function Pause() {
        NightlyTrex.runner.stop();
        appBar.showOnlyCommands(commands.paused);
      }

      function Play() {
        NightlyTrex.runner.play();
        appBar.showOnlyCommands(commands.playing);
      }

      function Restart() {
        NightlyTrex.runner.stop();
        NightlyTrex.runner.restart();
        appBar.showOnlyCommands(commands.playing);
      }

      function SaveScore() {
        saveScoreDialog.show();
      }

      function showAbout() {
        aboutDialog.show();
      }

      const SaveHiScore = (eventObject: any): any => {
        if (eventObject.detail.result === WinJS.UI.ContentDialog.DismissalResult.none) {
          eventObject.preventDefault();
          return;
        }
        if (eventObject.detail.result === WinJS.UI.ContentDialog.DismissalResult.primary) {
          return NightlyTrex.DataServices.saveScore(NightlyTrex.user.hiScore)
            .then((response: any) => {
              new MajesticWaffle.UI.Alert(null, { text: "New HiScore saved successfully", type: "success", duration: 7000 });
              NightlyTrex.gameOverDialogs[1].winControl.show();
              appBar.showOnlyCommands(["cmdPlay"]);
            })
            .catch((error: any) => {
              new MajesticWaffle.UI.Alert(null, { text: "Error occured while saving your hi score, please retry later", type: "warning", duration: 0 });
            });
        }
      };

      const logout = () => {
        NightlyTrex.DataServices.logoutUser()
          .then((response: any): any => {
            NightlyTrex.user = WinJS.Binding.as({
              id: 0,
              name: "UserLogin",
              avatar: "/assets/images/user.png",
              scores: new WinJS.Binding.List(),
              email: "",
              country: "",
              birthday: new Date(),
              hiScore: 0
            });
            return response;
          })
          .then((response: any) => {
            MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[0]);
          })
          .catch((error: any) => {
            new MajesticWaffle.UI.Alert(null, { text: error.data.error_description, type: "danger", duration: 0 });
          });
      };

      const goToProfile = () => MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[9]);

      const goFullscreen = () => screenfull.toggle(document.documentElement);

      let gameView = document.getElementById("game-view");

      let runnerContainer = document.createElement("div");

      runnerContainer.id = "trex-runner";

      gameView.appendChild(runnerContainer);

      NightlyTrex.runner = new Runner("#trex-runner");

      if (!NightlyTrex.runner.isPatched) {
        NightlyTrex.Utilities.trexPatch();
      }

      userCreditsTemplate.render(NightlyTrex.user, userMenuAnch);
      hiScoreTemplate.render(NightlyTrex.user, hiScoreContainer);

      userMenuAnch.addEventListener("click", function () {
        NightlyTrex.runner.stop();
        userMenu.show(userMenuAnch, "bottom", "left");
      });

      NightlyTrex.runner.outerContainerEl.addEventListener("gamestarted", function () {
        appBar.showOnlyCommands(commands.playing);
        tipsElement.innerHTML = tips.playing;
      });

      NightlyTrex.runner.outerContainerEl.addEventListener("gameplaying", function () {
        appBar.showOnlyCommands(commands.playing);
        tipsElement.innerHTML = tips.playing;
      });

      NightlyTrex.runner.outerContainerEl.addEventListener("gamerestarted", function () {
        appBar.showOnlyCommands(commands.playing);
        tipsElement.innerHTML = tips.playing;
      });

      NightlyTrex.runner.outerContainerEl.addEventListener("gamepaused", function () {
        if (NightlyTrex.runner.isGameOver) {
          return;
        }
        appBar.showOnlyCommands(commands.paused);
        tipsElement.innerHTML = tips.pause;
      });

      NightlyTrex.runner.outerContainerEl.addEventListener("gameover", function (e: any) {
        // let dialog = NightlyTrex.gameOverDialogs[NightlyTrex.randomDialogChoice(2)];
        appBar.showOnlyCommands(commands.gameOver);
        NightlyTrex.user.hiScore = Math.round(NightlyTrex.runner.highestScore * 0.025);
        tipsElement.innerHTML = tips.gameOver;
        // dialog.winControl.show();
      });

      appBar.getCommandById("cmdPlay").addEventListener("click", Play, false);
      appBar.getCommandById("cmdPause").addEventListener("click", Pause, false);
      appBar.getCommandById("cmdRestart").addEventListener("click", Restart, false);
      appBar.getCommandById("cmdSaveScore").addEventListener("click", SaveScore, false);

      saveScoreDialog.addEventListener("beforehide", SaveHiScore, false);

      document.getElementById("myprofile-command").addEventListener("click", goToProfile, false);
      document.getElementById("logout-command").addEventListener("click", logout, false);
      document.getElementById("fullscreen-command").addEventListener("click", goFullscreen, false);
      document.getElementById("about-command").addEventListener("click", showAbout, false);

      appBar.showOnlyCommands(["cmdPlay"]);
      tipsElement.innerHTML = tips.start;

      document.title = "Nightly T-REX | Home";

      if (options && options.account_created && !!options.account_created) {
        kiipInstance.postMoment("create_account");
      }

    }
  });
})();