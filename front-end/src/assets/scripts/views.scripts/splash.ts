let splash = WinJS.UI.Pages.define("/views/splash.html", {
  ready: function (elem: HTMLElement, options?: any): void {

    const errorCb = () => {
      NightlyTrex.DataServices.getCountries()
        .done((result: any) => {
          if (result.status === 200) {
            let fullCountries = result.response;
            NightlyTrex.countries = fullCountries.map((country: any) => { return { key: country.alpha2Code, value: country.name }; });
            MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[1]);
          }
        });
    };

    NightlyTrex.noRewardsDialog = document.getElementById("no-rewards-dialog");
    NightlyTrex.likePageDialog = document.getElementById("like-page-dialog");
    NightlyTrex.goShopDialog = document.getElementById("go-shop-dialog");

    NightlyTrex.gameOverDialogs = [NightlyTrex.likePageDialog, NightlyTrex.goShopDialog];

    // NightlyTrex.DataServices.getFrontProducts()
    //   .done((result: any) => {
    //     let frontProductsFlipView = document.getElementById("products-thumbs-flipview");
    //     if (result.status === 200) {
    //       NightlyTrex.Shop.frontProducts = new WinJS.Binding.List(result.response);
    //       frontProductsFlipView.winControl.itemDataSource = NightlyTrex.Shop.frontProducts.dataSource;
    //     }
    //   })

    NightlyTrex.DataServices.getCurrentUser()
      .then((userDetails: any) => {
        if (userDetails.data instanceof Object) {
          NightlyTrex.user.id = userDetails.data.id;
          NightlyTrex.user.avatar = userDetails.data.avatar;
          NightlyTrex.user.name = userDetails.data.login;
          NightlyTrex.user.email = userDetails.data.email;
          NightlyTrex.user.country = userDetails.data.country;
          NightlyTrex.user.birthday = userDetails.data.birthday;
          // kiipInstance.setEmail(userDetails.data.email);
          MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[4]);
        }
        else {
          errorCb();
        }
      })
      .catch((error: any) => {
        if (error.data) {
          errorCb();
          return;
        }
        new MajesticWaffle.UI.Alert(null, { text: "The application incountered a problem while loading, please try reloading the application. If the problem persists, try again later", type: "danger", duration: 0 });
      });

  }
});