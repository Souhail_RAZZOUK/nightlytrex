declare let ScoresListView: any;

(function () {

  let rank = 1;

  const getScoresItems = () => {
    return NightlyTrex.DataServices.getScoresChunk(NightlyTrex.currentScoresPage++)
      .then((scoresRes: any): any => {
        if (scoresRes.data.length > 0) {
          scoresRes.data.forEach(function (item: any) {
            let user = scoresRes.relatedObjects.users[item.user];
            NightlyTrex.scores.data.push({ ranking: rank++, avatar: user.avatar, name: user.login, score: parseFloat(item.score) });
          });
        }
        else {
          (NightlyTrex.currentScoresPage > 1) ? NightlyTrex.currentScoresPage-- : NightlyTrex.currentScoresPage;
        }
        return scoresRes;
      });
  };

  const getAllScores = () => NightlyTrex.DataServices.getScores()
    .then((result: any) => {
      result.data.forEach(function (item: any) {
        let user = result.relatedObjects.users[item.user];
        NightlyTrex.scores.data.push({ ranking: rank++, avatar: user.avatar, name: user.login, score: parseFloat(item.score) });
      });
    })
    .catch(() => new MajesticWaffle.UI.Alert(null, { text: "Error getting scores from Data Base", type: "danger", duration: 0 }));

  getAllScores();

  backand.on("hi_scores_added", () => {
    rank = 1;
    NightlyTrex.scores.data = new WinJS.Binding.List([]);
    getAllScores();
  });

  WinJS.Namespace.define("NightlyTrex", {
    currentScoresPage: 1,
    scoresListFooterVisibility: WinJS.UI.eventHandler(function (ev: any) {
      let visible = ev.detail.visible;
      let scoresListViewProgress = WinJS.Utilities.query(".footer .progress", ev.target)[0];
      let status = WinJS.Utilities.query(".footer .status", ev.target)[0];
      if (visible) {

        scoresListViewProgress.winControl.show();
        WinJS.UI.Animation.fadeIn(status);

        getScoresItems()
          // .then((result: any) => {
          //   if (result.data && result.data.length > 0) {
          //     WinJS.UI.Animation.fadeOut(status);
          //     scoresListViewProgress.winControl.hide();
          //   }
          // })
          .catch((error: any) => {
            new MajesticWaffle.UI.Alert(null, { text: "Error getting scores from Data Base", type: "danger", duration: 0 });
          });

      } else {
        WinJS.UI.Animation.fadeOut(status);
        scoresListViewProgress.winControl.hide();
      }
    })
  });


  let scores = WinJS.UI.Pages.define("/views/scores.html", {
    ready: function (elem: HTMLElement, options?: any): void {


      let scoresListContainer = document.getElementById("scores-list");
      let scoresListTemplate = document.getElementById("scores-list-template");

      // NightlyTrex.currentScoresPage = 1;
      // rank = 1;
      // NightlyTrex.scores.data = new WinJS.Binding.List();

      scoresListTemplate.winControl.render(NightlyTrex.scores, scoresListContainer)
        .done(() => {
          // let scoresListViewProgress = WinJS.Utilities.query(".footer .progress", scoresListContainer)[0];
          // let status = WinJS.Utilities.query(".footer .status", scoresListContainer)[0];

          WinJS.UI.processAll();

          // getScoresItems()
          //   .catch((error: any) => {
          //     WinJS.UI.Animation.fadeOut(status);
          //     scoresListViewProgress.winControl.hide();
          //     new MajesticWaffle.UI.Alert(null, { text: "Error getting scores from Data Base", type: "danger", duration: 0 });
          //   });
        });

    }
  });

})();