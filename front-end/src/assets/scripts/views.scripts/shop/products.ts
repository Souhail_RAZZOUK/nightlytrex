let productsPage = WinJS.UI.Pages.define("/views/shop/products.html", {
  ready: function (elem: HTMLElement, options?: any): void {
    let productsListView = document.getElementById("products-list");

    NightlyTrex.DataServices.getProducts()
      .done((result: any) => {
        if (result.status === 200) {
          let productsList = new WinJS.Binding.List(result.response);
          productsListView.winControl.itemDataSource = productsList.dataSource;
        }
      });
  }
});