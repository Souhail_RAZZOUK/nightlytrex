let changePassword = WinJS.UI.Pages.define("/views/account/profile.change-password.html", {
  ready: function (elem: HTMLElement, options?: any): void {

    let changePasswordSteps = document.getElementById("change-password-steps");
    let oldPasswordEntry = document.getElementById("old-password-entry");
    let newPasswordEntry = document.getElementById("new-password-entry");

    changePasswordSteps.winControl.onsubmit = function () {
      let entriesElements = changePasswordSteps.querySelectorAll(".entry");
      let entries = Array.from(entriesElements).map((entry: any) => entry.winControl);
      let errors = MajesticWaffle.Utilities.validateEntries(entries);

      if (MajesticWaffle.Utilities.containsError(errors)) {
        new MajesticWaffle.UI.Alert(null, { text: "There are errors in your entries, please correct them, then try again", type: "danger", duration: 5000 });
        return;
      }

      let oldPassword = oldPasswordEntry.winControl.value;
      let newPassword = newPasswordEntry.winControl.value;

      NightlyTrex.DataServices.changeUserPassword(oldPassword, newPassword).then((response: any) => {
          new MajesticWaffle.UI.Alert(null, { text: "Password changed successfully", type: "success", duration: 0 });
          MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[10]);
        })
        .catch((error: any) => {
          new MajesticWaffle.UI.Alert(null, { text: "Password not changed, please retry ...", type: "danger", duration: 0 });
        });
    };

    changePasswordSteps.winControl.oncancel = function () {
      MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[9]);
    };

  }

});