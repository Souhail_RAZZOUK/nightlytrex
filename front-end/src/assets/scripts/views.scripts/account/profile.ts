let profileAccount = WinJS.UI.Pages.define("/views/account/profile.html", {
  ready: function (elem: HTMLElement, options?: any): void {

    let userDetailsContainer = document.getElementById("user-details");
    let userDetailsTemplate = document.getElementById("user-details-template");
    WinJS.Utilities.empty(userDetailsContainer);


    NightlyTrex.DataServices.getCountries()
      .done((result: any) => {
        if (result.status === 200) {
          let fullCountries = result.response;
          NightlyTrex.countries = fullCountries.map((country: any) => { return { key: country.alpha2Code, value: country.name }; });
        }
      });

    userDetailsTemplate.winControl.render(NightlyTrex.user, userDetailsContainer)
      .then(() => NightlyTrex.DataServices.getCurrentUserScores().then((result: any) => NightlyTrex.user.scores = new WinJS.Binding.List(result.data)))
      .done(() => WinJS.UI.processAll());

  }

});