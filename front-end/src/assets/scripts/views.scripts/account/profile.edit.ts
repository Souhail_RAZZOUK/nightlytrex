let profileAccountEdit = WinJS.UI.Pages.define("/views/account/profile.edit.html", {
  ready: function (elem: HTMLElement, options?: any): void {

    let self = this;
    let editAccountSteps = document.getElementById("edit-account-steps");
    let profilePicturePreviewElement = <HTMLImageElement>document.getElementById("profile-picture-preview");
    let profilePictureEntry = document.getElementById("profile-picture-entry").winControl;
    let errors = new Array();
    const updateProfilePicture = () => {
      let reader = new FileReader();
      let file = profilePictureEntry._input._element.winControl._files[0];

      reader.addEventListener("load", function () {
        profilePictureEntry._value = profilePicturePreviewElement.src = reader.result;
      }, false);

      if (file) {
        reader.readAsDataURL(file);
      }
    };

    profilePicturePreviewElement.src = NightlyTrex.user.avatar;

    profilePictureEntry.addEventListener("change", updateProfilePicture);
    // profilePictureEntry._input._element.winControl.addEventListener("change", updateProfilePicturePreview);

    editAccountSteps.winControl.onsubmit = function () {
      let entriesElements = editAccountSteps.querySelectorAll(".entry");
      let entries = Array.from(entriesElements).map((entry: any) => entry.winControl);
      errors = MajesticWaffle.Utilities.validateEntries(entries);

      let newUser = {
        email: document.getElementById("email-entry").winControl.value || NightlyTrex.user.email,
        login: document.getElementById("user-name-entry").winControl.value || NightlyTrex.user.login,
        country: document.getElementById("country-entry").winControl.value || NightlyTrex.user.country,
        birthday: document.getElementById("birthday-entry").winControl.value || NightlyTrex.user.birthday,
        avatar: document.getElementById("profile-picture-entry").winControl.value || NightlyTrex.user.avatar
      };

      console.log(newUser);

      if (MajesticWaffle.Utilities.containsError(errors)) {
        new MajesticWaffle.UI.Alert(null, { text: "There are errors in your entries, please correct them, then try again", type: "danger", duration: 5000 });
        return;
      }

      // NightlyTrex.DataServices.loginUser(login, password)
      //   .then((response: any) => {
      //     MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[9]);
      //   })
      //   .catch((error: any) => {
      //     new MajesticWaffle.UI.Alert(null, { text: error.data.error_description, type: "danger", duration: 5000 });
      //   });
    };

    editAccountSteps.winControl.oncancel = function () {
      MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[9]);
    };

    WinJS.UI.processAll();

  }

});