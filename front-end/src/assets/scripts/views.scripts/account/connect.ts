let connectAccount = WinJS.UI.Pages.define("/views/account/connect.html", {
  ready: function (elem: HTMLElement, options?: any): void {

    let self = this;
    let connectsSteps = document.getElementById("connect-account-steps");
    let errors = new Array();

    connectsSteps.winControl.onsubmit = function () {
      let entriesElements = connectsSteps.querySelectorAll(".entry");
      let entries = Array.from(entriesElements).map((entry: any) => entry.winControl);
      errors = MajesticWaffle.Utilities.validateEntries(entries);

      if (MajesticWaffle.Utilities.containsError(errors)) {
        new MajesticWaffle.UI.Alert(null, { text: "There are errors in your entries, please correct them, then try again", type: "danger", duration: 5000 });
        return;
      }
      let login = document.getElementById("email-entry").winControl.value;
      let password = document.getElementById("password-entry").winControl.value;

      NightlyTrex.DataServices.loginUser(login, password)
        .then((response: any) => {
          MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[4]);
        })
        .catch((error: any) => {
          new MajesticWaffle.UI.Alert(null, { text: error.data.error_description, type: "danger", duration: 5000 });
        });
    };

    connectsSteps.winControl.oncancel = function () {
      MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[0]);
    };

  }

});