let createAccount = WinJS.UI.Pages.define("/views/account/create.html", {
  ready: function (elem: HTMLElement, options?: any): void {

    let self = this;
    let createsSteps = document.getElementById("create-account-steps");
    let entriesSummery = document.getElementById("summery");
    let errors = new Array();
    let editCallbacks = new Object({
      email: () => createsSteps.winControl.goToStep(1),
      password: () => createsSteps.winControl.goToStep(1),
      country: () => createsSteps.winControl.goToStep(2),
      birthday: () => createsSteps.winControl.goToStep(2),
      "user-name": () => createsSteps.winControl.goToStep(3),
      avatar: () => createsSteps.winControl.goToStep(3)
    });

    createsSteps.winControl.addEventListener("endreached", () => {
      let entriesElements = createsSteps.querySelectorAll(".entry");
      let entries = Array.from(entriesElements).map((entry: any) => entry.winControl);
      entriesSummery.winControl.callbacks = editCallbacks;
      entriesSummery.winControl.entries = entries;
    });

    createsSteps.winControl.onsubmit = function () {
      let entriesElements = createsSteps.querySelectorAll(".entry");
      let entries = Array.from(entriesElements).map((entry: any) => entry.winControl);
      errors = MajesticWaffle.Utilities.validateEntries(entries);

      if (MajesticWaffle.Utilities.containsError(errors)) {
        new MajesticWaffle.UI.Alert(null, { text: "There are errors in your entries, please correct them, then try again", type: "danger", duration: 5000 });
        return;
      }

      let newUser = {
        firstName: "Nightly",
        lastName: "Trex",
        email: document.getElementById("email-entry").winControl.value,
        date_joined: new Date().toUTCString(),
        password: document.getElementById("password-entry").winControl.value,
        confirmPassword: document.getElementById("password-entry").winControl.confirm.value,
        login: document.getElementById("user-name-entry").winControl.value,
        country: document.getElementById("country-entry").winControl.value,
        birthday: document.getElementById("birthday-entry").winControl.value,
        avatar: `https://api.adorable.io/avatars/${document.getElementById("user-name-entry").winControl.value}`
      };

      NightlyTrex.DataServices.signupUser(newUser)
        .then((response: any) => {
          let scenario = {
            url: NightlyTrex.scenarios[4].url,
            title: NightlyTrex.scenarios[4].title,
            state: {
              secured: true,
              account_created: true
            }
          };
          MajesticWaffle.Navigation.navigate(scenario);
        })
        .catch((error: any) => {
          console.log(error);
          new MajesticWaffle.UI.Alert(null, { text: error.data.error_description, type: "danger", duration: 0 });
        });
    };

    createsSteps.winControl.oncancel = function () {
      MajesticWaffle.Navigation.navigate(NightlyTrex.scenarios[0]);
    };
  }

});