Runner.prototype.isGameOver = false;

Runner.prototype.init = (function (original) {
  return function () {
    var result = original.apply(this, arguments);
    this.outerContainerEl.dispatchEvent(new Event('gameinitialized'));
    return result;
  }
})(Runner.prototype.init);

Runner.prototype.update = (function(original) {
  return function() {
    var result = original.apply(this,arguments);
    if (this.tRex.jumpCount == 1) {
      this.outerContainerEl.dispatchEvent(new Event('gamestarted'));
      this.update = original;
      return original;    
    }
  }
})(Runner.prototype.update);

Runner.prototype.play = (function (original) {
  return function () {
    var result = original.apply(this, arguments);
    Runner.prototype.isGameOver = false;
    this.outerContainerEl.dispatchEvent(new Event('gameplaying'));
    return result;
  }
})(Runner.prototype.play);

Runner.prototype.restart = (function (original) {
  return function () {
    var result = original.apply(this, arguments);
    Runner.prototype.isGameOver = false;
    this.outerContainerEl.dispatchEvent(new Event('gamerestarted'));
    return result;
  }
})(Runner.prototype.restart);

Runner.prototype.stop = (function (original) {
  return function () {
    var result = original.apply(this, arguments);
    this.outerContainerEl.dispatchEvent(new Event('gamepaused'));
    return result;
  }
})(Runner.prototype.stop);

Runner.prototype.gameOver = (function (original) {
  return function () {
    var result = original.apply(this, arguments);
    Runner.prototype.isGameOver = true;
    this.outerContainerEl.dispatchEvent(new Event('gameover'));
    return result;
  }
})(Runner.prototype.gameOver);
