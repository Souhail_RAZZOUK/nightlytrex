(function () {

  const trexPatch = (): void => {
    Runner.prototype.isGameOver = false;

    Runner.prototype.init = (function (original) {
      return function () {
        let result = original.apply(this, arguments);
        this.outerContainerEl.dispatchEvent(new Event("gameinitialized"));
        return result;
      };
    })(Runner.prototype.init);

    Runner.prototype.loadSounds = (function (original) {
      return function () {
        let result;
        try {
          result = original.apply(this, arguments);
        } catch (error) {
          new MajesticWaffle.UI.Alert(null, { text: "Sounds not enabled " + error.message, isInline: false, type: "danger", duration: 10000 });
        }
        return result;
      };
    })(Runner.prototype.loadSounds);

    Runner.prototype.update = (function (original) {
      return function () {
        let result = original.apply(this, arguments);
        if (this.tRex.jumpCount === 1) {
          this.outerContainerEl.dispatchEvent(new Event("gamestarted"));
          this.update = original;
          return original;
        }
      };
    })(Runner.prototype.update);

    Runner.prototype.play = (function (original) {
      return function () {
        let result = original.apply(this, arguments);
        Runner.prototype.isGameOver = false;
        this.outerContainerEl.dispatchEvent(new Event("gameplaying"));
        return result;
      };
    })(Runner.prototype.play);

    Runner.prototype.restart = (function (original) {
      return function () {
        let result = original.apply(this, arguments);
        Runner.prototype.isGameOver = false;
        this.outerContainerEl.dispatchEvent(new Event("gamerestarted"));
        return result;
      };
    })(Runner.prototype.restart);

    Runner.prototype.stop = (function (original) {
      return function () {
        let result = original.apply(this, arguments);
        this.outerContainerEl.dispatchEvent(new Event("gamepaused"));
        return result;
      };
    })(Runner.prototype.stop);

    Runner.prototype.gameOver = (function (original) {
      return function () {
        let result = original.apply(this, arguments);
        Runner.prototype.isGameOver = true;
        this.outerContainerEl.dispatchEvent(new Event("gameover"));
        return result;
      };
    })(Runner.prototype.gameOver);

    Runner.prototype.isPatched = true;
  };

  WinJS.Namespace.define("NightlyTrex.Utilities", {
    trexPatch: trexPatch,
  });
})();
