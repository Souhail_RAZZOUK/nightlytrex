/**
   * T-Rex runner.
   * @param {string} outerContainerId Outer containing element id.
   * @param {Object} opt_config
   * @constructor
   * @export
   */

declare class Runner {
  public outerContainerEl: HTMLElement;
  public isGameOver: boolean;
  public isPatched: boolean;
  public highestScore: number;
  constructor(outerContainerId: string, opt_config?: any);
  public init(): void;
  public loadSounds(): void;
  public update(): void;
  public play(): void;
  public stop(): void;
  public restart(): void;
  public gameOver(): void;
}

declare let screenfull: any;

/**
 * Main namespace
 */

declare namespace NightlyTrex {
  let scenarios: Array<any>;
  let currentScoresPage: number;
  let noRewardsDialog: HTMLElement;
  let likePageDialog: HTMLElement;
  let goShopDialog: HTMLElement;
  let gameOverDialogs: Array<HTMLElement>;
  let countries: Array<any>;
  let runner: Runner;
  let user: any;
  let scores: any;
  let Shop: any;

  function randomDialogChoice(max: number): number;
  /**
   * Binding (NightlyTrex) sub-namespace holder
   */

  namespace Binding {
    function displayDatesBind(source: any, sourceProperty: any, destination: any, destinationProperty: any): any;
  }

  /**
   * Utilities (NightlyTrex) sub-namespace holder
   */

  namespace Utilities {

    function trexPatch(): void;

  }

  /**
   * Data Services (NightlyTrex) sub-namespace holder
   */

  namespace DataServices {
    function init(): any;
    function signupUser(user: any): any;
    function loginUser(login: string, password: string): any;
    function logoutUser(): any;
    function getCurrentUser(): any;
    function getUser(id: string): any;
    function changeUserPassword(oldPassword: string, newPassword: string): any;
    function requestResetPassword(email: string): any;
    function resetPassword(newPassword: string, resetToken: string): any;
    function getUserScores(id: string): any;
    function getCurrentUserScores(): any;
    function getScoresChunk(page: number): any;
    function getScores(): any;
    function saveScore(hiScore: number): any;
    function getCountries(): any;
    function getProducts(): any;
    function getFrontProducts(): any;
  }

}