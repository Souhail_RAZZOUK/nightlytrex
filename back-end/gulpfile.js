var gulp        = require('gulp'),
    gutil       = require('gulp-util'),
    concat       = require('gulp-concat'),
    typescript  = require('gulp-typescript'),
    tslint      = require('gulp-tslint')
    tsl         = require("tslint"),
    del         = require('del'),
    merge       = require('merge2'),
    path        = require('path');

var tsProject = typescript.createProject("./tsconfig.json");

gulp.task('ts', function() {
  var program = tsl.Linter.createProgram("./tsconfig.json");
  var tsResult = gulp.src('src/**/*.ts')
                  .pipe( tsProject());

  return merge([
      tsResult.dts.pipe(gulp.dest('bin/js/')),
      tsResult.js.pipe(gulp.dest('bin/js/')),
    ]);
});

gulp.task('bundle', function() {
  return gulp.src(['./bin/js/dependencies.js', './bower_components/backand-vanilla-sdk/dist/backand.js', './bin/js/utilities.js', './bin/js/main.js'])
    .pipe(concat('server.js'))
    .pipe(gulp.dest('bin/'));
});

gulp.task('tslint', function() {
  return gulp.src('src/**/*.ts')
    .pipe( tslint({
      formatter: 'stylish'
    }))
    .pipe( tslint.report({
      emitError: false
    }))
})

gulp.task('clean', function () {
	del(['bin/**/*', '!bin/', '!bin/bower_components', '!bin/bower_components/**/*', '!bin/bower.json', '!bin/assets/js/**/*'], {force: true}).then(paths => {
    if(paths.length != 0){
      gutil.log('Files and folders that were deleted:\n', gutil.colors.orange(paths.join('\n')));
    }
  });
});

gulp.task('watch', function () {

  gulp.watch('src/**/*.ts',['tslint', 'ts']);

  gulp.watch('bin/js/*.js',['bundle']);

});

gulp.task('default', ['tslint', 'ts', 'bundle', 'watch']);