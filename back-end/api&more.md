# Avatars: https://api.adorable.io/avatars/list

{"face":{"eyes":["eyes1","eyes10","eyes2","eyes3","eyes4","eyes5","eyes6","eyes7","eyes9"],"nose":["nose2","nose3","nose4","nose5","nose6","nose7","nose8","nose9"],"mouth":["mouth1","mouth10","mouth11","mouth3","mouth5","mouth6","mouth7","mouth9"]}}

# DB

[
  {
    "name": "highscores",
    "fields": {
      "score": {
        "type": "float"
      },
      "datescored": {
        "type": "datetime"
      },
      "user": {
        "object": "users"
      }
    }
  },
  {
    "name": "users",
    "fields": {
      "highscores": {
        "collection": "highscores",
        "via": "user"
      },
      "email": {
        "type": "string"
      },
      "firstName": {
        "type": "string"
      },
      "lastName": {
        "type": "string"
      },
      "login": {
        "type": "string"
      },
      "password": {
        "type": "string"
      },
      "country": {
        "type": "string"
      },
      "birthday": {
        "type": "datetime"
      },
      "avatar": {
        "type": "string"
      }
    }
  }
]

``[
  {
    "name": "hi_scores",
    "fields": {
      "score": {
        "type": "string"
      },
      "date_recorded": {
        "type": "datetime"
      },
      "user": {
        "object": "users"
      }
    }
  },
  {
    "name": "users",
    "fields": {
      "hi_scores": {
        "collection": "hightScores",
        "via": "user"
      },
      "email": {
        "type": "string"
      },
      "firstName": {
        "type": "string"
      },
      "lastName": {
        "type": "string"
      },
      "date_joined": {
        "type": "datetime"
      },
      "login": {
        "type": "string"
      },
      "password": {
        "type": "string"
      },
      "country": {
        "type": "string"
      },
      "birthday": {
        "type": "datetime"
      },
      "avatar": {
        "type": "string"
      }
    }
  }
]``

# WT

- editing link :
  *  https://webtask.it.auth0.com/edit/wt-souhail_razzouk-hotmail_com-0#/server/eyJhbGciOiJIUzI1NiIsImtpZCI6IjIifQ.eyJqdGkiOiJjYTg3YmE4YzU4MzA0YWJkYmQwYjE5YzZlYjFmYjQzNCIsImlhdCI6MTQ3MzgxMDg3OCwiY2EiOlsiOTAwNzMzNGRiMDhjNGQ2M2E0MTNjZGFmM2YzYjYxNGMiXSwiZGQiOjEsInRlbiI6Ii9ed3Qtc291aGFpbF9yYXp6b3VrLWhvdG1haWxfY29tLVswLTFdJC8ifQ.G7yUMxR4NSkaVrvdC9mOUOURv1QSRtF513ZqLfpBbf4

# Routes

- "/login" 

- "/logout"

- "/signup"

- "/checklogin"

- "/user"

- "/hiscores" 

# MVCAS

``
{
  // `data` is the response that was provided by the server
  data: {},

  // `status` is the HTTP status code from the server response
  status: 200,

  // `statusText` is the HTTP status message from the server response
  statusText: 'OK',

  // `headers` the headers that the server responded with
  headers: {},

  // `config` is the config that was provided to `bknd` for the request
  config: {}
}
``

# Legacy:

var dataService = {
  get: function(options, callback) {
    var data = {};
    var dataCallback = (res) => {
        var temp = '';
        
        res.on('data', (chunk) => {
          temp += chunk;
        });
        
        res.on('end', () => {
          data = JSON.parse(temp);
          callback(data);
        });
      }
    var errorCallback = (e) => {
        data = e;
      }
    
    if(options.protocol === 'https:'){
      https.get(options, dataCallback).on('error', errorCallback);
    }else {
      http.get(options, dataCallback).on('error', errorCallback);
    }
  },
  
  getAsync: function(requests, callback) {
    var asyncTasks = [];
  
    requests.forEach(function(request){
      asyncTasks.push(function(){
        dataService.get(request.options, function(data){
          request.results = data;
          callback();
        });
      });
    });
    
    async.parallel(asyncTasks, function(){
      callback();
    });
  }
}
